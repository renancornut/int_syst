# Objectifs projet

Concernant mon projet fil-rouge, j'ai décidé d'utiliser comme solution technique Ansible AWX.

En explorant les différentes possibilités que m'offre AWX, j'ai décidé de créer une infrastructure en utilisant gns3 et virtualbox, qui sera totalement orchestrée par AWX, où chaque VM sera créee par vagrant et configuré par AWX. Les routeurs et les Switches seront également configurés entièrement par AWX.

Mon objectif est donc d'automatiser entièrement le déploiement d'une infra à travers des workflows de playbook et maitriser au maximum les outils que j'ai à disposition.

De plus, je pourrais également utiliser mes compétences en réseau afin de créer une infra un minimum complexe en intégrant différents protocoles vu en cours.


## Axes de réflexion 
 
Créer une VM avec un serveur web apache2 entièrement configuré sans taper la moindre commande sur son terminal

Intégrer une automatisation des sauvegardes dans mes playbooks

Intégrer un système de centralisation des logs où je transférerai les logs de chaques machines vers un point unique

Pouvoir redeployer mon infra en moins de 20 min
