# Sauvegarde et restauration


## Installation ansible-tower-cli

Pour pouvoir manipuler awx depuis le terminal linux, il faut installer via pip le packet Tower-cli
Cet outils permet de la récupération, créer, modifier et supprimer des ressources sur L'API

    pip install ansible-tower-cli

Il n'y a pas de man pour ce packet, il faut utiliser :

    tower-cli --help

Il faut ensuite permettre à tower cli d'accèder à notre awx, pour ça il faut taper :

    tower-cli config host  http://10.0.0.2:80

Il faut ensuite que tower cli puisse se log sur awx pour le manipuler

    tower-cli config username admin
    tower-cli config password password

Mettre la vérification SSl en false

    tower-cli config verify_ssl False

Pour vérifier votre config :

    tower-cli config

![](./Images/configuration.png)

On peut faire toutes ces config sur le fichier ~/.tower_cli.cfg

![](./Images/Fichier_de_conf.png)

## Sauvegarde API

Une fois toutes tower_cli configuré, on peut lancer la sauvegarde avec la commande :

    tower-cli receive --all >  nomduFichier.json

Ce fichier Json va contenir toutes les données au format json de awx (projets,utilisateur,inventaire, references et Jobs)

## Sauvegarde BDD PostegreSQL

La base de donnée postgre tourne sur un conteneur docker

Pour faire une sauvegarde de toute la database, il faut d'abord stopper tous les conteneurs sauf  celui qui contient postgre

![](./Images/Conteneurs_à_stopper.png)

    docker stop <-nom_du_conteneur>

![](./Images/docker_stop.png)

On peut ensuite sauvegarder la BDD

    docker exec -t -u postgres 6be8cc0611d1 pg_dumpall -c > postgretest.sql

Dans mon cas, j'ai un message d'erreur dans le dossier sql que je n'arrive pas à résoudre

![](./Images/Error.png)

## Restauration

Pour faire une restauration, il suffit de selectionner le fichier de sauvegarde json avec la commande ci-dessous

    tower-cli send  nomduFichier.json

![](./Images/Backup.png)

J'ai crée un script en bash afin de sauvegarder automatiquement grâce à crontab mon fichier json contenant toute la conf de awx

[Script bash sauvegarde](./Script/Sauvegarde_awx)